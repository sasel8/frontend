/* todo-input sınıfına sahip html öğesini seçerek todo-input değişkenine atar.*/
const todoInput = document.querySelector(".todo-input"); /*değer girilmesi için*/
const todoButton = document.querySelector(".todo-button");
const todoList = document.querySelector(".todo-list");
const filterOption = document.querySelector(".filter-todo");/*açılır menü için*/

document.addEventListener("DOMContentLoaded", getLocalTodos);

/*butona tıklama eylemi ekler. addTodo fonksiyonu çağrılır*/
todoButton.addEventListener("click",addTodo);

todoList.addEventListener("click",deleteCheck);
/* seçenekler değiştiğinde filtertodo fonk çağrılır*/
filterOption.addEventListener("change",filterTodo);

function addTodo(event){
    event.preventDefault();/*yeniden yüklemeyi önle-dinamik yapı*/

    const todoDiv = document.createElement("div");
    todoDiv.classList.add("todo");

    /*yeni bir li öğesi oluştururlarak kullanıcının girdiği metni ekle*/
    const newTodo= document.createElement("li");
    newTodo.innerText=todoInput.value;

    newTodo.classList.add("todo-item");
    todoDiv.appendChild(newTodo);/* li öğesini div öğesine ekle*/

    
/*görev yazıldığında basılması gereken buton */
    const completedButton=document.createElement("button");/*buton oluştur*/
    completedButton.innerHTML='<i class="fas fa-check-circle"></i>';/*buton içeriği*/
    completedButton.classList.add("complete-btn");
    todoDiv.appendChild(completedButton);/*görünüm sağlanır*/

    /*silme butonu oluşturuluyor*/
    const trashButton=document.createElement("button");
    trashButton.innerHTML='<i class="fas fa-trash"></i>';/*çöp kutusu html ikonu*/
    trashButton.classList.add("trash-btn");/*css ile bağlanarak görünüm sağla*/
    todoDiv.appendChild(trashButton);

    todoList.appendChild(todoDiv);
    todoInput.value="";/*ınput satırını temizler*/

}

function deleteCheck(e){
    const item=e.target;

    if(item.classList[0]==="trash-btn"){ /*tıklanan öğe trash-btn sınıfına aitse*/
        const todo=item.parentElement;
        todo.classList.add("slide");

        todo.addEventListener("transitionend", function() {
            todo.remove();
        });
    }

       if(item.classList[0]==="complete-btn"){
        const todo=item.parentElement;
        todo.classList.toggle("completed");
       }
    }

    function filterTodo(e){
        const todos=todoList.childNodes;/*tüm todolist öğelerini al*/
        todos.forEach(function(todo){
            switch(e.target.value){
                case "all":
                    todo.style.display="flex";
                    break;
                case "completed":
                    if(todo.classList.contains("completed")){
                        todo.style.display="flex";
                    } else{
                        todo.style.display="none";
                    }
                    break;
                case "incomplete":
                    if(!todo.classList.contains("completed")){
                        todo.style.display="flex";
                    } else{
                        todo.style.display="none";
                    }
                    break;
            }
        });
    }


function saveLocalTodos(todo){
    let todos;
    if(localStorage.getItem("todos")===null){
        todos=[];
    }else{
        todos=JSON.parse(localStorage.getItem("todos"));
    }
    todos.push(todo);
    localStorage.setItem("todos", JSON.stringify(todos));
}

function getLocalTodos(){
    let todos;/*yerel depo dizisi*/
    if(localStorage.getItem("todos")===null){
        todos=[];
    }else{
        todos=JSON.parse(localStorage.getItem("todos"));

}

todos.forEach(function(todo) {
    const todoDiv=document.createElement("div");/*her bir görev için div oluştur*/
    todoDiv.classList.add("todo");

    const newTodo=document.createElement("li");
    newTodo.innerText=todo; 
    
    newTodo.classList.add("todo-item");
    todoDiv.appendChild(newTodo);

    const completedButton =document.createElement("button");
    completedButton.innerHTML='<i class="fas fa-check-circle"></i>';
    completedButton.classList.add("complete-btn");
    todoDiv.appendChild(completedButton);


    const trashButton =document.createElement("button");
    trashButton.innerHTML='<i class="fas fa-trash"></i>';
    trashButton.classList.add("trash-btn");
    todoDiv.appendChild(trashButton);

    
});

}

function removeLocalTodos(todo){
    let todos;
    if(localStorage.getItem("todos")===null){
        todos= [];
    } else{
        todos=JSON.parse(localStorage.getItem("todos"));
    }

    const todoIndex=todo.children[0].innerText;
    todos.splice(todos.indexOf(todoIndex), 1);
    localStorage.getItem("todo", JSON.stringify(todos));
}